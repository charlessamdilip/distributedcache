# Distributed Cache
Distributes cache system with individual http server ports to set and retrieve cache internally communicating
through vertx event bus and managed by HazleCast.

## Assignment
> #Distributed Cache System-
>
> “A key-value store, or key-value database, is a data storage paradigm designed for storing, retrieving, and managing associative arrays, a data structure more commonly known today as a dictionary or hash” ~ Wikipedia.
>
> ##Task Overview -
>
> We would like you to implement a distributed Key-Value (KV) store using any language of your choice.
>
> For simplicity you can assume both the key and value types are strings. Your KV store should be running as (at least) 2 different processes that replicate data between them (i.e) we should be able to put in a Key and Value to Process 1 and query for the same Key on Process 2, for which we should get the corresponding Value.
>
>
> We would like you to expose the store via a HTTP service that would allow us to GET / SET key-value pairs. Ideally, write it as you would a production piece of code with tests. Bonus points for making this as fast as possible.
>
> Sample Invocations
>
> $ curl -H "Content-type: application/json" -XPOST http://localhost:4455/set/key -d ‘“value”’
>
> OK
>
> $ curl -H “Accept: application/json” http://localhost:4466/get/key
>
> “value”
>
> Evaluation Criteria -
>
> Correctness of functionality. We would be verifying your implementation with requests to validate that it is able to store and retrieve the data correctly on both single process and across processes too.
>
> Tests. We expect you to write tests for your code.
>
> Code Modularity. Please don’t over engineer your solution at the same time, writing the entire solution in one big method / function is also not desirable. Please use your > discretion to design your code to be modular enough for possible extensions.



## Prerequisites
- jdk 1.8
- maven

## Build
- mvn clean package -DskipTests

## Run
- Make sure the conf in src/main/resources is done correctly
- run `java -jar target/distributedcache-1.0-SNAPSHOT-fat.jar`
- Play around with the port sepecifid in the conf
 - curl -XPOST -H "Content-Type: text/plain"  -H "Accept: text/plain"-d "&lt;value&gt;" http://localhost:&lt;port&gt;/set/&lt;key&gt;
 - curl -XGET -H "Accept: text/plain" http://localhost:&lt;port&gt;/get/&lt;key&gt;

## Test Run
- Build, then start deploy fresh verticles in port 8000 and 8001
- run `mvn -Dtest=VerticleTest test`

## API Documentation:
For each node there are two apis:

### /get/:key
> HTTP GET method, Retrieves for the key from store and returns in Content-Type "text/plain"
>
> - `status: 200 and value in the body` if the key present
> - `status: 404 and message "Not Found"` in the body

### /set/:key
> HTTP POST method, both accepts and returns Content-Type "text/plain"
> always accept a value in the body and key in parameter and propagates the data pair into the cluster returns
> - `status: 202 and message "OK"` in the body


## Note
> - All the content type and accept should be "text/plain"
> - Cache will replicated only with the same cluster name in conf
> - Test may fail if there is a lag in eventbus but data will be in sync as the update happens on timestamp.