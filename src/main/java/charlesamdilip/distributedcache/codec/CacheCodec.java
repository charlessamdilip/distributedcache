package charlesamdilip.distributedcache.codec;

import charlesamdilip.distributedcache.model.KeyValueCache;
import charlesamdilip.distributedcache.model.ValueObject;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonObject;

import java.util.Map;

public class CacheCodec  implements MessageCodec<KeyValueCache, KeyValueCache> {
  @Override
  public void encodeToWire(Buffer buffer,KeyValueCache map) {
    JsonObject jsonObject = new JsonObject();
    // Get fields
    for (Map.Entry<String, ValueObject> pair : map.entrySet()) {
      JsonObject valueObjectJson = new JsonObject();

      ValueObject valueObject = pair.getValue();
      valueObjectJson.put("key", valueObject.getKey());
      valueObjectJson.put("value", valueObject.getValue());
      valueObjectJson.put("timeStamp", valueObject.getTimestamp());

      jsonObject.put(pair.getKey(), valueObjectJson);
    }

    // Encode object to string
    String jsonToStr = jsonObject.encode();

    // Length of JSON: is NOT characters count
    int length = jsonToStr.getBytes().length;

    // Write data into given buffer
    buffer.appendInt(length);
    buffer.appendString(jsonToStr);
  }

  @Override
  public KeyValueCache decodeFromWire(int i, Buffer buffer) {
    int _pos = i;

    // Length of JSON
    int length = buffer.getInt(_pos);

    // Get JSON string by it`s length
    // Jump 4 because getInt() == 4 bytes
    String jsonStr = buffer.getString(_pos+=4, _pos+=length);
    JsonObject contentJson = new JsonObject(jsonStr);

    KeyValueCache map = new KeyValueCache();
    // Get fields
    for (Map.Entry<String, Object> pair : contentJson) {
      JsonObject valueObjectJson = (JsonObject)pair.getValue();
      ValueObject valueObject = new ValueObject();
      valueObject.setKey(valueObjectJson.getString("key"));
      valueObject.setValue(valueObjectJson.getString("value"));
      valueObject.setTimestamp(valueObjectJson.getLong("timeStamp"));
      map.put(pair.getKey(), valueObject);
    }

    return map;
  }

  @Override
  public KeyValueCache transform(KeyValueCache map) {
    return map;
  }

  @Override
  public String name() {
    return this.getClass().getName();
  }

  @Override
  public byte systemCodecID() {
    return -1;
  }
}
