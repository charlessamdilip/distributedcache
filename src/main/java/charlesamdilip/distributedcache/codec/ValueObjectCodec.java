package charlesamdilip.distributedcache.codec;

import charlesamdilip.distributedcache.model.ValueObject;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonObject;

public class ValueObjectCodec implements MessageCodec<ValueObject, ValueObject> {
  @Override
  public void encodeToWire(Buffer buffer, ValueObject valueObject) {
    JsonObject jsonToEncode = new JsonObject();
    jsonToEncode.put("key", valueObject.getKey());
    jsonToEncode.put("value", valueObject.getValue());
    jsonToEncode.put("timeStamp", valueObject.getTimestamp());

    // Encode object to string
    String jsonToStr = jsonToEncode.encode();

    // Length of JSON: is NOT characters count
    int length = jsonToStr.getBytes().length;

    // Write data into given buffer
    buffer.appendInt(length);
    buffer.appendString(jsonToStr);
  }

  @Override
  public ValueObject decodeFromWire(int i, Buffer buffer) {
    int _pos = i;

    // Length of JSON
    int length = buffer.getInt(_pos);

    // Get JSON string by it`s length
    // Jump 4 because getInt() == 4 bytes
    String jsonStr = buffer.getString(_pos+=4, _pos+=length);
    JsonObject contentJson = new JsonObject(jsonStr);

    // Get fields
    ValueObject valueObject = new ValueObject();
    valueObject.setKey(contentJson.getString("key"));
    valueObject.setValue(contentJson.getString("value"));
    valueObject.setTimestamp(contentJson.getLong("timeStamp"));

    return valueObject;
  }

  @Override
  public ValueObject transform(ValueObject valueObject) {
    return valueObject;
  }

  @Override
  public String name() {
    return this.getClass().getName();
  }

  @Override
  public byte systemCodecID() {
    return -1;
  }
}
