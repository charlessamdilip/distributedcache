package charlesamdilip.distributedcache.model;

public class ValueObject {
  private String key;
  private String value;
  private long timestamp;

  public String getValue() {
    return value;
  }

  public String getKey() {
    return key;
  }

  public ValueObject setKey(String key) {
    this.key = key;
    return this;
  }

  public ValueObject setValue(String value) {
    this.value = value;
    return this;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public ValueObject setTimestamp(long timestamp) {
    this.timestamp = timestamp;
    return this;
  }
}
