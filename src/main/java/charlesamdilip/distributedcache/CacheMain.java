package charlesamdilip.distributedcache;

import charlesamdilip.distributedcache.codec.CacheCodec;
import charlesamdilip.distributedcache.codec.ValueObjectCodec;
import charlesamdilip.distributedcache.model.KeyValueCache;
import charlesamdilip.distributedcache.model.ValueObject;
import charlesamdilip.distributedcache.verticle.CacheVerticle;
import com.hazelcast.config.Config;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.impl.VertxImpl;
import io.vertx.core.impl.cpu.CpuCoreSensor;
import io.vertx.core.json.JsonObject;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CacheMain {

  /**
   * Deploys the Verticle for cache in cluster mode
   *
   * @param args
   * @author Charles Sam Dilip J
   */
  public static void main(String ...args) {
    try {
      // Reads the cluster configurations
      Scanner scanner = (new Scanner(new File("src/main/resources/deployment_conf.json"), "UTF-8"))
          .useDelimiter("\\A");
      JsonObject deploymentConf = new JsonObject(scanner.next());

      // Initializing the Cluster Manager
      Config config = new Config();
      config.getGroupConfig().setName(deploymentConf.getString("cluster.name"));
      HazelcastClusterManager mgr = new HazelcastClusterManager();
      mgr.setConfig(config);
      VertxOptions options = new VertxOptions().setClusterManager(mgr);

      // Deploy the vertx in cluster mode
      Vertx.clusteredVertx(options, res -> {
        if (res.succeeded()) {
          Vertx vertx = res.result();
          // Registering the codec
          vertx.eventBus().registerDefaultCodec(ValueObject.class, new ValueObjectCodec()); // default for update data
          vertx.eventBus().registerDefaultCodec(KeyValueCache.class, new CacheCodec()); // default for sync data
          if ((((VertxImpl) vertx).getClusterManager()).getNodes().size() == 1) {
            deploymentConf.put("singleNode", true);
          } else {
            deploymentConf.put("singleNode", false);
          }

          // Deploy the verticle
          vertx.deployVerticle(CacheVerticle.class.getName(), new DeploymentOptions()
              .setInstances(CpuCoreSensor.availableProcessors())
              .setConfig(deploymentConf), atsAsyncResult -> {
            if (atsAsyncResult.succeeded()) {
              System.out.println("Deployed the cache on port " + deploymentConf.getLong("http.port") +
                  " in the cluster " + deploymentConf.getString("cluster.name"));
            } else {
              System.out.println("Unable to the deploy cache with conf(cluster_name, port): " +
                  deploymentConf.getString("cluster.name") + "," + deploymentConf.getLong("http.port"));
              System.exit(-1);
            }
          });
        } else {
          System.out.println("Unable to Cluster");
        }
      });
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }
}
