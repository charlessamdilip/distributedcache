package charlesamdilip.distributedcache.verticle;

import charlesamdilip.distributedcache.model.KeyValueCache;
import charlesamdilip.distributedcache.model.ValueObject;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.Calendar;

public class CacheVerticle extends AbstractVerticle {
  private KeyValueCache keyValueMap;
  private boolean initialized;

  @Override
  public void start(Future<Void> fut) throws Exception {
    super.start();

    // Initializing the Cache
    initialized = false;
    if (null != this.config().getBoolean("init.sync") && this.config().getBoolean("init.sync") &&
        !this.config().getBoolean("singleNode")) {
      addSyncUpDataListener();
      this.vertx.eventBus().publish(this.config().getString("cluster.name") + "._initSync", "");
    } else {
      keyValueMap = new KeyValueCache();
      initialized = true;
    }
    addSyncListener();
    addUpdateListener();

    // Initializing the http server
    Router router = Router.router(this.vertx);
    router = addExceptionHandler(router);
    router = addGetterHandler(router);
    router = addSetterHandler(router);
    deployServer(router, fut);
  }

  /**
   * Updates the cluster nodes for any value into the system
   */
  private void addUpdateListener() {
    this.vertx.eventBus().consumer(this.config().getString("cluster.name") + "._update").handler(message -> {
      ValueObject valueUpdateObject = (ValueObject) message.body();
      ValueObject valueObject = keyValueMap.get(valueUpdateObject.getKey());

      if (null == valueObject) { // Case key not found
        keyValueMap.put(valueUpdateObject.getKey(), valueUpdateObject);
      } else {
        if (valueUpdateObject.getTimestamp() > valueObject.getTimestamp()) {
          keyValueMap.put(valueUpdateObject.getKey(), valueUpdateObject);
        }
      }
    });
  }

  /**
   * Updates the cluster nodes for any value into the system
   */
  private void addSyncListener() {
    this.vertx.eventBus().consumer(this.config().getString("cluster.name") + "._initSync").handler(message -> {
      if (this.initialized) {
        this.getVertx().eventBus().publish(this.config().getString("cluster.name") + "._syncData",
            keyValueMap);
      }
    });
  }

  private void addSyncUpDataListener() {
    this.vertx.eventBus().consumer(this.config().getString("cluster.name") + "._syncData").handler(message -> {
      if (!this.initialized) {
        this.keyValueMap = (KeyValueCache) message.body();
        this.initialized = true;
      }
    });
  }

  /**
   * Add the Exception Handler
   * @param router
   * @return
   */
  private Router addExceptionHandler(Router router) {
    router.route().failureHandler(routingContext -> {
      writeTextResponse(routingContext, 500, "Internal Server Error");
    });
    return router;
  }

  /**
   * Getter Api - gets the value for the key from the store
   * @param router
   * @return
   */
  private Router addGetterHandler(Router router) {
    // Getter API
    router.get("/get/:key").produces("text/plain").pathRegex("/get/(?<key>.+)")
        .handler(routingContext -> {
          String key = routingContext.request().params().get("key");
          ValueObject valueObject = keyValueMap.get(key);
          // Case key not found
          if (null == valueObject) {
            writeTextResponse(routingContext, 404,  "Not Found");
            return;
          }

          writeTextResponse(routingContext, 200, valueObject.getValue());
        });
    return router;
  }

  /**
   * Setter Api - sets the value for the key-value to the store
   * @param router
   * @return
   */
  private Router addSetterHandler(Router router) {
    // Setter API
    router.post("/set/:key").consumes("text/plain").produces("text/plain").pathRegex("/set/(?<key>.+)")
        .handler(BodyHandler.create());
    router.post("/set/:key").consumes("text/plain").produces("text/plain").pathRegex("/set/(?<key>.+)")
        .handler(routingContext -> {
          EventBus eventBus = routingContext.vertx().eventBus();
          ValueObject valueObject = new ValueObject().setKey(routingContext.request().params().get("key"))
              .setValue(routingContext.getBodyAsString())
              .setTimestamp(Calendar.getInstance().getTimeInMillis());
          // Publishes the update to the cluster nodes (includes itself)
          eventBus.publish(this.config().getString("cluster.name") + "._update", valueObject);
          writeTextResponse(routingContext, 202, "OK");
        });
    return router;
  }

  /**
   * Http server deployment on the port
   * @param router
   * @param fut
   */
  private void deployServer(Router router, Future<Void> fut) {
    this.vertx.createHttpServer()
        .requestHandler(router::accept)
        .listen(
            // Retrieve the port from the configuration,
            this.config().getInteger("http.port"),
            result -> {
              if (result.succeeded()) {
                fut.complete();
              } else {
                fut.fail(result.cause());
              }
            }
        );
  }

  /**
   * Http text response writer
   *
   * @param routingContext
   * @param code
   * @param message
   */
  private void writeTextResponse(RoutingContext routingContext, int code, String message) {
    routingContext.response().setStatusCode(code)
        .putHeader("Content-Type", "application/text; charset=utf-8")
        .end(message);
  }
}
