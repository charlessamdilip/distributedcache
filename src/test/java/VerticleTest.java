import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class VerticleTest {

  private Vertx vertx;
  private HttpClient httpClient;
  private HttpClient httpClient8001;

  @Before
  public void setUp(TestContext context) {
    vertx = Vertx.vertx();
    HttpClientOptions httpClientOptions = new HttpClientOptions().setDefaultHost("localhost")
        .setDefaultPort(8000);
    HttpClientOptions httpClientOptions8001 = new HttpClientOptions().setDefaultHost("localhost")
        .setDefaultPort(8001);
    httpClient = vertx.createHttpClient(httpClientOptions);
    httpClient8001 = vertx.createHttpClient(httpClientOptions);
  }

  @After
  public void tearDown(TestContext context) {
    httpClient.close();
    vertx.close(context.asyncAssertSuccess());
  }

  @Test
  public void testInitGet(TestContext context) {

    final Async async = context.async();
    httpClient.getNow("/get/key",
        response -> {
          if (response.statusCode() == 404) {
            response.handler(body -> {
              context.assertTrue(body.toString().equals("Not Found"));
              async.complete();
            });
          } else {
            context.assertTrue(false);
            async.complete();
          }
        });
  }

  @Test
  public void testPostAndGet(TestContext context) {

    final Async async = context.async();
    httpClient.post("/set/key1", httpClientResponse -> {
      if (httpClientResponse.statusCode() == 202) {
        httpClient.getNow("/get/key1",
            response -> {
              if (response.statusCode() == 200) {
                response.handler(body -> {
                  context.assertTrue(body.toString().equals("value1"));
                  async.complete();
                });
              } else {
                context.assertTrue(false);
                async.complete();
              }
            });
      } else {
        context.assertTrue(false);
        async.complete();
      }
    }).setChunked(true).putHeader("Content-Type", "text/plain").putHeader("Accept", "text/plain")
    .end("value1");
  }

  @Test
  public void testPostMultiple(TestContext context) {

    final Async async = context.async();
    httpClient.post("/set/key2", httpClientResponse -> {
      if (httpClientResponse.statusCode() == 202) {
        httpClient.post("/set/key3", httpClientResponse1 -> {
          if (httpClientResponse1.statusCode() == 202) {
            httpClient.getNow("/get/key2",
                response -> {
                  if (response.statusCode() == 200) {
                    response.handler(body -> {
                      if (body.toString().equals("value2")) {
                        httpClient.getNow("/get/key3",
                            response1 -> {
                              if (response1.statusCode() == 200) {
                                response1.handler(body1 -> {
                                  context.assertTrue(body1.toString().equals("value3"));
                                  async.complete();
                                });
                              } else {
                                context.assertTrue(false);
                                async.complete();
                              }
                            });
                      } else {
                        context.assertTrue(false);
                        async.complete();
                      }
                    });
                  } else {
                    context.assertTrue(false);
                    async.complete();
                  }
                });
          } else {
            context.assertTrue(false);
            async.complete();
          }
        }).setChunked(true).putHeader("Content-Type", "text/plain").putHeader("Accept", "text/plain")
            .end("value3");
      } else {
        context.assertTrue(false);
        async.complete();
      }
    }).setChunked(true).putHeader("Content-Type", "text/plain").putHeader("Accept", "text/plain")
        .end("value2");
  }


  @Test
  public void updateKey(TestContext context) {
    final Async async = context.async();
    httpClient.post("/set/key4", httpClientResponse -> {
      if (httpClientResponse.statusCode() == 202) {
        httpClient.post("/set/key5", httpClientResponse1 -> {
          if (httpClientResponse1.statusCode() == 202) {
            httpClient.getNow("/get/key4",
                response -> {
                  if (response.statusCode() == 200) {
                    response.handler(body -> {
                      if (body.toString().equals("value4")) {
                        httpClient.getNow("/get/key5",
                            response1 -> {
                              if (response1.statusCode() == 200) {
                                response1.handler(body1 -> {
                                  if (body1.toString().equals("value5")) {
                                    httpClient.post("/set/key4", httpClientResponse2 -> {
                                      if (httpClientResponse2.statusCode() == 202) {
                                        httpClient.getNow("/get/key4",
                                            response2 -> {
                                              if (response2.statusCode() == 200) {
                                                response2.handler(body2 -> {
                                                  context.assertTrue(body2.toString().equals("value6"));
                                                  async.complete();
                                                });
                                              } else {
                                                context.assertTrue(false);
                                                async.complete();
                                              }
                                            });
                                      } else {
                                        context.assertTrue(false);
                                        async.complete();
                                      }
                                    }).setChunked(true).putHeader("Content-Type", "text/plain").putHeader("Accept", "text/plain")
                                        .end("value6");;
                                  } else {
                                    context.assertTrue(false);
                                    async.complete();
                                  }
                                  async.complete();
                                });
                              } else {
                                context.assertTrue(false);
                                async.complete();
                              }
                            });
                      } else {
                        context.assertTrue(false);
                        async.complete();
                      }
                    });
                  } else {
                    context.assertTrue(false);
                    async.complete();
                  }
                });
          } else {
            context.assertTrue(false);
            async.complete();
          }
        }).setChunked(true).putHeader("Content-Type", "text/plain").putHeader("Accept", "text/plain")
            .end("value5");
      } else {
        context.assertTrue(false);
        async.complete();
      }
    }).setChunked(true).putHeader("Content-Type", "text/plain").putHeader("Accept", "text/plain")
        .end("value4");
  }

  @Test
  public void testPost8001AndGetInBoth(TestContext context) {
    final Async async = context.async();
    httpClient8001.post("/set/key7", httpClientResponse -> {
      if (httpClientResponse.statusCode() == 202) {
        httpClient.getNow("/get/key7",
            response -> {
              if (response.statusCode() == 200) {
                response.handler(body -> {
                  if (body.toString().equals("value7")) {
                    httpClient8001.getNow("/get/key7",
                        response1 -> {
                          if (response1.statusCode() == 200) {
                            response1.handler(body1 -> {
                              context.assertTrue(body.toString().equals("value7"));
                              async.complete();
                            });
                          } else {
                            context.assertTrue(false);
                            async.complete();
                          }
                        });
                  } else {
                    context.assertTrue(false);
                    async.complete();
                  }
                });
              } else {
                context.assertTrue(false);
                async.complete();
              }
            });
      } else {
        context.assertTrue(false);
        async.complete();
      }
    }).setChunked(true).putHeader("Content-Type", "text/plain").putHeader("Accept", "text/plain")
        .end("value7");
  }

}
